<?php

namespace Sas;

class Log {

  static function create($data, $name = '', $path = 'assets/log') {
    ob_start();
    print_r($data);
    $plain = ob_get_contents();
    ob_end_clean();
    if ($handle = fopen($_SERVER['DOCUMENT_ROOT'] . '/' . $path . "/log_" . microtime() . $name . ".txt", 'w+')) {
      fwrite($handle, $plain . "\n");
      fclose($handle);
    }
  }

}
